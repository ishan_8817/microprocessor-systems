/*
LAB01 
ISHAN GOYAL
22331035
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/float.h"
#include "pico/double.h"

/**
 * @brief Calculate the value of PI using the Wallis product algorithm
 *        with single-precision floating-point representation.
 * 
 * @return float The calculated value of PI.
 */
float wallis_pi_single_precision() {
    float pi = 1.0f;
    for (int i = 1; i <= 100000; i++) {
        float numerator = 4.0f * i * i;
        float denominator = numerator - 1.0f;
        pi *= numerator / denominator;
    }
    return 2.0f * pi;
}

/**
 * @brief Calculate the value of PI using the Wallis product algorithm
 *        with double-precision floating-point representation.
 * 
 * @return double The calculated value of PI.
 */
double wallis_pi_double_precision() {
    double pi = 1.0;
    for (int i = 1; i <= 100000; i++) {
        double numerator = 4.0 * i * i;
        double denominator = numerator - 1.0;
        pi *= numerator / denominator;
    }
    return 2.0 * pi;
}

int main() {

#ifndef WOKWI
    // Initialize the serial interface to communicate with the computer
    // Only required for hardware and not needed for simulation in Wokwi
    stdio_init_all();
#endif

    // Calculate and print the value of PI using single-precision representation
    float pi_single = wallis_pi_single_precision();
    printf("PI (single-precision): %f\n", pi_single);

    // Calculate and print the approximation error for single-precision representation
    float pi_single_error = fabsf(pi_single - M_PI);
    printf("PI approximation error (single-precision): %f\n", pi_single_error);

    // Calculate and print the value of PI using double-precision representation
    double pi_double = wallis_pi_double_precision();
    printf("PI (double-precision): %lf\n", pi_double);

    // Calculate and print the approximation error for double-precision representation
    double pi_double_error = fabs(pi_double - M_PI);
    printf("PI approximation error (double-precision): %lf\n", pi_double_error);

    // Return zero to indicate that the program executed successfully.
    return 0;
}
